package br.sistema.edu;


public class ItemCompra {
	
	
	private float preco;
	private String descricao;
	private int quantidadeItem;
	
	
	private Produto produto = new Produto();
	
	public ItemCompra(String descricao, float preco, int quantidade) {
			this.preco = preco;
			this.descricao = descricao;
			this.quantidadeItem = (int) quantidade;
	}

	public float getPreco() {
		return preco;
	}

	public void setPreco(float preco) {
		this.preco = preco;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public int getQuantidadeItem() {
		return quantidadeItem;
	}

	public void setQuantidadeItem(int quantidadeItem) {
		this.quantidadeItem = quantidadeItem;
	}

	public Produto getProduto() {
		return produto;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}
	
}
