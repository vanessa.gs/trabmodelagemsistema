package br.sistema.edu;

public class Endereco {
	
	private String rua;
	private int numero;
	private String cep;
	private String cidade;
	private String uf;
	private TipoEndereco tipo;
	
	
	public Endereco(String rua, String cep, String cidade, String uf, int numero, TipoEndereco tipo) {
		this.rua = rua;
		this.cep = cep;
		this.cidade = cidade;
		this.uf = uf;
		this.numero = numero;
		this.tipo = tipo;
}


	public String getRua() {
		return rua;
	}


	public void setRua(String rua) {
		this.rua = rua;
	}


	public int getNumero() {
		return numero;
	}


	public void setNumero(int numero) {
		this.numero = numero;
	}


	public String getCep() {
		return cep;
	}


	public void setCep(String cep) {
		this.cep = cep;
	}


	public String getCidade() {
		return cidade;
	}


	public void setCidade(String cidade) {
		this.cidade = cidade;
	}


	public String getUf() {
		return uf;
	}


	public void setUf(String uf) {
		this.uf = uf;
	}


	public TipoEndereco getTipo() {
		return tipo;
	}


	public void setTipo(TipoEndereco tipo) {
		this.tipo = tipo;
	}
	
}
