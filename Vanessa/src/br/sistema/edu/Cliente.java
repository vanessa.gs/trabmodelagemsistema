package br.sistema.edu;

import java.util.ArrayList;
import java.util.List;
import java.util.Date ;

public class Cliente {
	
	private String nome;
	private String cpf;
	private String telefone;
	private Date dtCadastro;
	

	private List<Compra>compras= new ArrayList<Compra>();
	
	public void adicionarCompra(Compra compra) {
		compras.add(compra);
	}
	
	public void removerCompraPorIdProduto(int idProduto) {
		for (Compra compra: compras) {
			if(compra.getIdProduto()== idProduto) {
				compras.remove(idProduto);
		}
	}
}
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public Date getDtCadastro() {
		return dtCadastro;
	}

	public void setDtCadastro(Date dtCadastro) {
		this.dtCadastro = dtCadastro;
	}


	public Compra pegarCompraPorIdProduto(int idProduto) {
		for(Compra compra: compras) {
			if(compra.getIdProduto()== idProduto) {
				return compra;
			}
		}
	
		return null;
	}
	
	public List<Compra> getCompras(){
		return compras;
	}
}